function [] = convert_ambdec_configs(in_dir, out_dir)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    
    %%
    if ~exist('in_dir', 'var') || isempty(in_dir)
        in_dir = '/Users/heller/src/fons/ambdec-0.5.1/presets';
    end
    
    if ~exist('out_dir', 'var') || isempty(out_dir)
        out_dir = in_dir;
    end
    
    in_names = dir(fullfile(in_dir, '*.ambdec'));
    
    for i = 1:length(in_names)
        [pathstr, name, type] = fileparts(in_names(i).name);
        fprintf('converting %s\n', name);
        in_file = fullfile(in_names(i).folder, in_names(i).name);
        out_file = fullfile(out_dir, [name '.dsp']);
        ambdec2faust(in_file, out_file);
    end
    
    
end

