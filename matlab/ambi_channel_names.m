function names = ambi_channel_names(sh, letter_limit, name_format)
    
    global adt
    
    if ~exist('letter_limit', 'var') || isempty(letter_limit)
        letter_limit = 3;
    end
    
    if letter_limit > 3
        warning('No letter names above 3, using numbers');
        letter_limit = 3;
    end
    
    if ~exist('name_format', 'var') || isempty(name_format)
        if isfield(adt, 'channel_name_format')
            name_format = adt.channel_name_format;
        else
            name_format = '%d%d%c';
        end
    end
    
    fuma_ch_names = {...
        'W',...
        'Y', 'Z', 'X', ...
        'V', 'T', 'R', 'S', 'U', ...
        'Q', 'O', 'M', 'K', 'L', 'N', 'P'};
    sc = {'S','C','C'};
    
    acn = ambi_ACN(sh.l, sh.m);
    names = cell(size(acn));
    
    names(sh.l<=letter_limit) = fuma_ch_names(acn(sh.l<=letter_limit)+1);
    
    for i = acn(sh.l>letter_limit)
        names{acn==i} = sprintf( name_format, ...
            sh.l(acn==i), ...
            abs(sh.m(acn==i)), ...
            sc{sign(sh.m(acn==i))+2});
    end
    
end
