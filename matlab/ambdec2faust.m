function [] = ambdec2faust( ambdec_config, out_path, description, C_in )
    %UNTITLED2 Summary of this function goes here
    %   Detailed explanation goes here
    
    %% defaults
    [pathstr, name, ~] = fileparts(ambdec_config);
    if ~exist('out_path', 'var') || isempty(out_path)
        out_path = fullfile(pathstr, [name, '.dsp']);
    end
   
    %%
    [M,D] = ambdec2mat(ambdec_config);
    
    if ~exist('description','var') || isempty(description)
        D.description = [ name, ', ', D.ambdec_description];
    end
    
    C_coeffs = ambi_channel_definitions(D.hor_order, D.ver_order, 'HV', ...
        D.coeff_order, D.coeff_scale);
    
    if ~exist('C_in', 'var') || isempty(C_in)
        C_in = ambi_channel_definitions(D.hor_order, D.ver_order, 'HV', ...
        D.coeff_order, D.input_scale);
    end
    
    M_adapter = ambi_make_adapter_matrix(C_in, C_coeffs);
    
    if isnumeric(M)
        M = M * M_adapter;
    else
        M.lf = M.lf * M_adapter;
        M.hf = M.hf * M_adapter;
    end

    D.input_scale = C_in.encoding_convention;
    D.coeff_scale = C_in.encoding_convention;
            
    S = ambdec2spkr_array(ambdec_config);
    
    write_faust_config(out_path, D, S, M, C_in);
end

