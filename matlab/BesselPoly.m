function [ p, r, err ] = BesselPoly( deg, rev )
    %BESSELPOLY Construct the bessel polynomial and find the roots
    %  poly = BesselPoly(degree) returns Bessel Polynomial of degree
    %  [p, r, err] = BesselPoly(degree, reverse) also finds roots and errors
    %
    % deg   is the degree of the polynomial
    % rev   return the Reverse Bessel Polynomial, see [4]
    %
    % poly  is the polynomial in MATLAB representation 
    % roots the array of the roots
    % err   is the abolute and relative errors
    %
    % See [1] for a discussion of how Bessel polynomials occur in the
    % theory of traveling spherical waves and their relation to spherical
    % Bessel functions, which are the radial part of the Fourier-Bessel
    % expansion of the sound field.
    %
    % See Secs 2.3, 3.2, and Fig 9 of [2] for how this applies to Ambisonic
    % near-field compensation (NFC) filters.
    %
    %% References:
    %
    % [1]   H. L. Krall and O. Frink, "A New Class of Orthogonal
    %       Polynomials: The Bessel Polynomials," Transactions of the
    %       American Mathematical Society, vol. 65, no. 1, pp. 100?115,
    %       Jan. 1949.
    %
    % [2]   J. Daniel, "Spatial Sound Encoding Including Near Field Effect:
    %       Introducing Distance Coding Filters and a Viable, New Ambisonic
    %       Format," Preprints 23rd AES International Conference,
    %       Copenhagen, 2003. (see note above)
    %
    % [3]   Weisstein, Eric W. "Bessel Polynomial." From MathWorld--A
    %       Wolfram Web Resource.
    %       http://mathworld.wolfram.com/BesselPolynomial.html
    %
    % [4]   Wikipedia contributors. "Bessel filter." Wikipedia, The Free
    %       Encyclopedia. Wikipedia, The Free Encyclopedia, 20 Sep. 2017.
    %       Web. 20 Nov. 2017.
    %
    % NOTE ABOUT [2]:*
    %
    % There is an error in Daniel's definition of the reverse Bessel
    % polynomial in Eqn 24 in [2] resulting in roots that are twice the
    % correct value. The definition here is correct. This has been
    % confirmed with Daniel. In additon, Daniel uses the bilinear transform
    % to convert the filter into digital form, which is inappropriate as it
    % does not preserve the phase response of the filter [see 4 (Digital)].
    % In the ADT implementation, I use a cascade of first- and second-order
    % digital state variable filters which preserves the amplitude and
    % phase response, as well as having better numerical performance.
    %
    % NOTE ABOUT ACCURACY:
    % MATLAB's root function is good to about 40th degree, but the Bessel
    % polynomial is ill-conditioned by the mid-20s. We check by evaluating
    % the polynomial at the calculated roots and reporting the absolute and
    % relative errors and issue a warning if the relative error is greater
    % than eps('single').
    
    %{
This file is part of the Ambisonic Decoder Toolbox (ADT).
Copyright (C) 2013, 2017  Aaron J. Heller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.
    
You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
    %}
    
    %% build the Bessel polynomial
    % In MATLAB a polynomial is a row vector of coefficients in order of
    % decreasing degree, so [3,0,3,1] is 3*x^3 + 3*x + 1
    
    term_degree = deg:-1:0; % the degree of each term
    % p = arrayfun(@coeff, term_degree);
    % octave says:
    %   error: handles to nested functions are not yet supported
    
    % workaround... good for MATLAB and Octave
    p = arrayfun(...
        @bessel_coeff, ...
        deg(ones(size(term_degree))), ...
        term_degree);
    
    %% for a filter, we want the reverse Bessel polynomial, see [4]
    if exist('rev', 'var') && rev
        p = fliplr(p);
    end
    
    %% find and check the roots
    if nargout > 1
        r = roots(p);
        
        % check the roots
        v = polyval(p, r);
        abs_error = max(abs(v(:)));
        % max/min is the condition number of the matrix
        rel_error = abs_error/(max(p)/min(p));
        
        if rel_error > eps('single')
            warning('excessive error %d', rel_error);
        end
        
        err = [max(abs(v(:))), rel_error];
        
    end
    
    % nice but doesn't work in Octave currently, left here for future use
    function a = coeff(term)
        if term == 0  %remove dependancy that prod([]./[]) = 1
            a = 1;
        else
            %a = factorial(m+i)/factorial(m-i)/factorial(i)/2^i;
            
            % this is better numerically
            a = nchoosek(deg,term) * ...
                prod( (deg+(1:term)) ./ linspace(2,2,term) );
        end
    end
end

function a = bessel_coeff(degree, term)
    
    if term == 0  %remove dependancy that prod([]./[]) = 1
        a = 1;
    else
        %a = factorial(m+i)/factorial(m-i)/factorial(i)/2^i;
        
        % this is better numerically
        a = nchoosek(degree,term) * ...
            prod( (degree+(1:term)) ./ linspace(2,2,term) );
    end
end

