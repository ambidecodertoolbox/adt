function [f] = factorial_quotient(num, dem)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    
    if num < 0 || dem < 0
        error('num and dem should be non-negative')
    end
    
    if num > dem
        f = prod( (dem+1):num );
    elseif num < dem
        f = 1/prod( (num+1):dem );
    else
        f = 1;
    end
end

%{
def factorial_quotient(num, dem):
    "returns num!/dem!, but computed to prevent overflow"

    if num < 0 or dem < 0:
        raise ValueError("num and dem should be non-negative")

    if num > dem:
        return Rational(prod(range(dem + 1, num + 1)), 1)
    elif num < dem:
        return Rational(1, prod(range(num + 1, dem + 1)))
    else:
        return Integer(1)
        
        %}
