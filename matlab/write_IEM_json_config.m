function [] = write_IEM_json_config( filename, D, S, M, C )
    %WRITE_IEM_JSON_CONFIG - write ADT results as an IEM JSON file
    
    % FILENAME - output file for JSON, if empty write to console
    % D - decoder struct returned by ambi_run_* function
    % S - speaker struct
    % M - decoder matrix
    % C - channel definition struct
    
    % writes the results of an ADT decoder computation to a JSON file
    
    % example invocation:
    % >> adt_initialize;
    
    
    
    %% argument defaults
    if isempty(filename)
        fid = 1; % write to console
    else
        [fid,msg] = fopen(filename,'w');
        if msg
            error([msg ': ' filename]);
        end
    end
    
    %% check for compatible channel definition
    % FIXME: for now throw an error, eventually convert 
    if strcmpi(C.ordering_rule, 'acn') && ...
            strcmpi(C.encoding_convention, 'sn3d')
    else
        error('unhandled convention channel definition: order: %s, normalization %s', ...
            C.ordering_rule, C.encoding_convention);
    end
        
    %% make sure jsonlab is on the path
    if ~exist('savejson', 'file')
       addpath(fullfile(ambi_dir('matlab'), 'jsonlab-1.5'));
    end
    
    fprintf('Writing IEM plug-in suite JSON config to: %s\n', filename);
    
    %% Configuration
    O = struct();
    [user, host, host_type] = getUserHost;
    
    O.Name = 'Ambisonic Decoder and loudspeaker layout';
    O.Description = ['Written by ' ambi_toolbox_version_string() ', run by ' user ' on ' host ' ( ' host_type ' ) at ' datestr(now)];
    
    %% Decoder
    
    switch D.freq_bands
        case 1
            matrix = M;
        case 2
            matrix = M.hf;
        otherwise
            error('freq_bands must be 1 or 2, not %i', D.freq_bands);
    end
    
    O.Decoder.Name = '';
    O.Decoder.Description = '';
    O.Decoder.ExpectedInputNormalization = 'sn3d';
    O.Decoder.Weights = 'maxrE';
    O.Decoder.WeightsAlreadyApplied = false;
    O.Decoder.Matrix = matrix;
      

    %% SPEAKERS
    for i = 1:length(S.id)
        lsp.Azimuth = S.az(i)*180/pi;
        lsp.Elevation = S.el(i)*180/pi;
        lsp.Radius = S.r(i);
        lsp.Channel = i;
        lsp.IsImaginary = false;
        lsp.Gain = 1.0;
        O.LoudspeakerLayout.Loudspeakers{i} = lsp;
        clear lsp
    end

      
    
    %% DECODER MATRIX
%     map = C.channels > -1; %hack to build true vector with correct length
%     
%     switch D.freq_bands
%         case 1
%             fprintf(fid, '\n#DECODERMATRIX\n');
%             write_rows(fid, ambi_apply_gamma(M,D.gains,C), map);
%             fprintf(fid, '#END\n');
%         case 2
%             fprintf(fid, '\n#DECODERMATRIX\n');
%             % M.hf does not have gamma applied at this point, so we must do
%             % it here.  (FIXME: why is this?)
%             write_rows(fid, ambi_apply_gamma(M.hf,D.hf_gains, C), map);
%             fprintf(fid, '#END\n');
%         otherwise
%             error('freq_bands must be 1 or 2, not %i', D.freq_bands);
%     end
    
    %% Encode and making it look nicer
    if false
        output = jsonencode(O);
        output = strrep(output, '{', '\n{\n');
        output = strrep(output, '}', '\n}');
        output = strrep(output, '":', '" : ');
        output = strrep(output, ',"', ',\n"');
        output = strrep(output, '],', '],\n');
    else
        output = savejson('', O);
    end
    
    fprintf(fid, output);
    if fid ~=1
        fclose(fid);
    end
    
end

function write_rows(fid, M, map)
    for i = 1:size(M,1)
        %fprintf(fid, 'add_row ');
        if ~exist('map','var') || isempty(map)
            fprintf(fid, '\t% f', M(i,:));
        else
            fprintf(fid, '\t% f', M(i,map));
        end
        fprintf(fid, '\n');
    end
end

