function [Sout] = ambi_spkr_array_join(array_name, varargin)
    %UNTITLED3 Summary of this function goes here
    %   Detailed explanation goes here
   
    Sout = varargin{1};
    
    if ~isempty(array_name)
        Sout.name = array_name;
    end
    
    for i = 2:length(varargin)
        S = varargin{i};
        Sout.id = [Sout.id(:)', S.id(:)'];
        Sout.az = [Sout.az(:); S.az(:)];
        Sout.el = [Sout.el(:); S.el];
        Sout.r = [Sout.r(:); S.r(:)];
        Sout.x = [Sout.x(:); S.x(:)];
        Sout.y = [Sout.y(:); S.y(:)];
        Sout.z = [Sout.z(:); S.z(:)];
    end
        
end

