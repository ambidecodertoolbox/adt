function [M_norm, diffuse_gain] = ambi_normalize_decoder(M, S, C)
    %AMBI_NORMALIZE_DECODER(M, S, C) - scale decoder for unity diffuse field gain
    %   experimental
        
%{
This file is part of the Ambisonic Decoder Toolbox (ADT).
Copyright (C) 2018  Aaron J. Heller

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

    a = LebedevGrid(32);
    a.w = a.w / (4*pi); % convert to unit normalization
    
    Y = ambi_sample_Y_sph(a.theta, a.phi, C)';
    
    M_hf = ambi_apply_gamma(M, ambi_shelf_gains(C, S,'rms' ), C);
    M_hf = M;
    
    [rV, rE, P, E] = ambi_rVrE_Y(M_hf, [S.x, S.y, S.z]', Y);
    
    pressure_gain = sum(P * a.w);
    energy_gain = sum(E * a.w);
    diffuse_gain = energy_gain;
    
    M_norm = M_hf / diffuse_gain;
    
end

