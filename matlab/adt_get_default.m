function [value, found] = adt_get_default(key, default)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    global adt

    found = isfield(adt, key);
    if found
        value = adt.(key);
    else
        value = default;
    end
end

