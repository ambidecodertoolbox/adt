function [ ch_mask ] = build_channel_mask_table( max_order, mixed_order_scheme )
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    
    %% defaults
    if ~exist('max_order', 'var') || isempty(max_order)
        max_order = 3;
    end
    
    if ~exist('mixed_order_scheme', 'var') || isempty(mixed_order_scheme)
        mixed_order_scheme = 'HV';
    end
    
    %%
    ch_mask_array = zeros((max_order+1)^2, 3);
    i = 0;
    for h_order = 1:max_order
        for v_order = 0:h_order
            C = ambi_channel_definitions(h_order, v_order, ...
                mixed_order_scheme, 'acn');
            w = 2.^C.index;
            m = sum(w(C.mask));
            fprintf('%dH%dV %d %x\n', h_order, v_order, m, m);
            i = i + 1;
            ch_mask_array(i, 1) = m;
            ch_mask_array(i, 2) = h_order;
            ch_mask_array(i, 3) = v_order;
        end
    end
    ch_mask_array(ch_mask_array(:,1)==0,:) = [];
    ch_mask.h = sparse(ch_mask_array(:,1), 1, ch_mask_array(:,2));
    ch_mask.v = sparse(ch_mask_array(:,1), 1, ch_mask_array(:,3));
end

