function [ outdir ] = ambi_decoders_dir( create )
    %AMBI_DECODERS_DIR(create) returns directory to which decoders are written
    %   create if true create the directory, else warn
    %   Edit this file to change where decoders are written
    
    global adt
    
    if isfield(adt, 'decoder_dir') && ~isempty(adt.decoder_dir)
        outdir = adt.decoder_dir;
    else
        outdir = fullfile('..','decoders');
    end
    
    if ~exist(outdir,'dir')
        if create
            mkdir(outdir);
        else
            warning('Decoder outdir does not exist: %s', outdir);
        end
    end
    
end

