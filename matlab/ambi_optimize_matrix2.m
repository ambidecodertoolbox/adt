function [ x ] = ambi_optimize_matrix2( M, C, S, test_dirs )
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    
    if ~exist('test_dirs', 'var') || isempty(test_dirs)
        test_dirs = load(fullfile(ambi_dir('data'), ...
            'Design_5200_100_random.mat'));
        test_dirs.u = [test_dirs.x, test_dirs.y test_dirs.z]';
        test_dirs.Y = ambi_sample_Y_sph(test_dirs.az, test_dirs.el, C)';
        fprintf('loading test_dirs');
    end
    
    if ~exist('dag_builder', 'file')
        addpath('/Users/heller/Documents/MATLAB/AutoDiff/')
    end
    
    n_test_dirs = length(test_dirs.az);
    Su = [ S.x, S.y, S.z ]';
    
    % directional weighting
    %full sphere
    w = ones(size(test_dirs));
    % upper hemi, plus a bit
    w = test_dirs.el >= 0;
    w = w';
    w_sum = sum(w(:));
    
    global DAG DDAG
    [DAG,DDAG] = generate_dags(@compute_fom) ;
    
    options = optimoptions('fmincon',...
        'SpecifyObjectiveGradient', true ...
        ,'Display','iter-detailed');
    %options = optimoptions( options, 'CheckGradients', true);
    options = optimoptions(options, 'StepTolerance', 1e-20);
    options = optimoptions(options, 'ConstraintTolerance', 1e-10);
    
    fun = @cost;
    x0 = M;
    A = [];
    b = [];
    Aeq = [];
    beq = [];
    lb = zeros(size(M))-1;
    ub = zeros(size(M))+1;
    
    nonlcon = [];
    while true
        fx0 = compute_fom(x0);
        x = fmincon(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options);
        fx = compute_fom(x);
        
        % zero out small coeficients 
        small = abs(x) > 0 & abs(x) < (max(x(:))/1000);
        fprintf(' #small = %d\n', sum(small(:)));
        if ~any(small(:))
            break;
        end
        x(small) = 0;
        lb(small) = 0;
        ub(small) = 0;
        x0 = x;
    end
    
    1; % place to put a breakpoint
    % end ambi_optimize_matrix
    
    function [f, g] = cost(M)
        f = evaluate_dag(DAG, M);
        [~, g] = evaluate_dag(DDAG, M, 1);
        g = g.';
    end
    
    function fom = compute_fom(M)
        
        g = M * test_dirs.Y(:,w);
        
        % velocity localization vector, rV
        if false
            P = sum(g,1);
            %rV.xyz = real((Su * g) ./ P([1 1 1], :)); % assume g is real
            rV.xyz = ((Su * g) ./ P([1 1 1], :)); % assume g is real
            rV.r = vector_magnitude(rV.xyz.^2);
            rV.u = rV.xyz ./ rV.r([1 1 1], :);
        end
        
        g2 = g.^2;
       
        E = sum(g2, 1);
        
        % rE (column vectors)
        rE.xyz = (Su * g2) ./ E([1 1 1], :);
        % magnitude of rE
        rE.r = sqrt(sum(rE.xyz.^2, 1));
        % unit vector in direction or rE
        rE.u = rE.xyz ./ rE.r([1 1 1], :);
        
        % resolve to longitudinal and transverse
        rE.uL = vector_dot(rE.u, test_dirs.u(:,w));
        rE.uT = sqrt(1 - rE.uL.^2);
        
        rE.rL = rE.uL .* rE.r;
        rE.rT = rE.uT .* rE.r;
        
        CE = sum((1 - E).^2, 2) ./ w_sum;
        CL = sum((0.9062 - rE.rL).^2, 2) ./ w_sum;
        CT = sum(rE.rT, 2) ./ w_sum;
        
        cE = 1000; 
        cL = 10000; 
        cT = 1000;
        
        fom = cE*CE + cL*CL + cT*CT;
        
        % prints during numerical execution, but not symbolic
        if size(M,1) > 1
            fprintf('rE mean=%d min=%d max=%d\n',...
                mean(rE.r), min(rE.r), max(rE.r));
            fprintf('%d %d %d %d\n', fom, cE*CE, cL*CL, cT*CT)
        end
        
    end
    
end

function val = vector_dot( A, B )
    val = sum(A .* B, 1);
end

function val = vector_magnitude(A)
    val = sqrt( sum(A.^2, 1) );
end

function [DAG,DDAG] = generate_dags(fn)
    % function [DAG,DDAG] = generate_dags(fn)
    
    DAG_B1 = dag_builder();     % create a blank dag_builder
    DAG_BN = build_dag(DAG_B1,fn);    % process the blank to generate the final
    DAG = get_dag(DAG_BN);      % extract the dag from the dag_ builder
    clear DAG_B1 DAG_BN      % clear the dag_builder objects
    lf = get_dag_size(DAG);
    
    % set the exits (passing back inter1nediate values
    ex= false(lf,1);
    ex(lf) = true;
    set_exits(DAG,ex);
    
    % start differentiating
    DDAG = duplicate_dag(DAG);
    DDAG_D = dag_radiff(DDAG);
    gen_helper(DDAG_D);
    build_radiff_dag(DDAG_D);
    
    % pull the stuff out
    DDAG = get_dag(DDAG_D);
    dex = get_exits(DDAG);
    dex(end) = 1;
    set_exits(DDAG,dex)
    
end

