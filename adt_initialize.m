%% script to initialize the Ambisonic Decoder Toolbox

% ---------- NOTE -----------
% DO NOT EDIT THIS FILE. IT MAY BE OVERWRITTEN BY UPDATES
% If you want to change defaults, in the folder adt/local, copy the file
% adt_initialize_local_EXAMPLE.m to adt_initialize_local.m and make changes
% in that file.
% ----------

clear adt
global adt

[install_dir, ~, ~ ] = fileparts(mfilename('fullpath'));
adt.install_dir = install_dir;

addpath(install_dir, fullfile(install_dir, 'matlab'));
for d = {'examples', 'data', 'local'}
    addpath(ambi_dir(d{1}));
end

adt_set_defaults( ...
    'matlab_version', ver(), ...
    'version', '1.2');

adt_set_defaults( ...
    'channel_order', 'acn', ...
    'channel_normalization', 'sn3d', ...
    'mixed_order_scheme', 'HV', ...
    'channel_name_format', '%d.%d%c', ...
    'lf_hf_g0', 'rms', ...
    'decoder_input_fullset', 0);

adt_set_defaults( ...
    'do_plots', ~inOctave(), ...
    'plots_visible', 'on', ...
    'decoder_dir', ambi_dir('decoders'));

% plotly
if ~inOctave()
    plotly_path = fullfile(install_dir, 'matlab', 'plotly');
    if exist(plotly_path, 'dir')
        try
            addpath(genpath(plotly_path));
            getplotlyoffline('https://cdn.plot.ly/plotly-latest.min.js');
            fprintf('Plotly initialized\n');
        catch e
            disp(e)
        end
    end
end

if exist('adt_initialize_local.m', 'file')
    fprintf('\nRunning local initializations.\n')
    adt_initialize_local; % octave gets an error if () here.
else
    1;
end

clear d install_dir
fprintf('\nADT initialized.\n')
    
