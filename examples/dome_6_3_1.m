function [D] = dome_6_3_1()
    %UNTITLED2 Summary of this function goes here
    %   Detailed explanation goes here
    
    radius = 1; %meter
    ele = 45;
    
    S = ambi_spkr_array(...
        'dome_6_3_1', ...
        'AER', 'DDM', ...
        'C',  [   0, 0, radius], ...
        'LF', [  60, 0, radius], ...
        'LB', [ 120, 0, radius], ...
        'B',  [ 180, 0, radius], ...
        'RB', [-120, 0, radius], ...
        'RF', [ -60, 0, radius], ...
        'LU', [  60, ele, radius], ...
        'RU', [ -60, ele, radius], ...
        'BU', [ 180, ele, radius], ...
        'T',  [   0, 90, radius]);
        
    C = ambi_channel_definitions_convention([2,2], 'ambix');
    
    D = ambi_run_allrad(S, C);
    
end

