function [ D, S, M, C ] = run_dec_coulter( )
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    
    S = ambi_spkr_array('c71', 'RAE', 'IDD', ...
        'L', [105,  30, 0], ...
        'R', [105, -30, 0], ...
        'C', [ 91,   0, 0], ...
        'SL', [60,  70, 0], ...
        'SR', [71, -70, 0], ...
        'RL', [61, 150, 0], ...
        'RR', [55,-150, 0]);
    
    C = ambi_channel_definitions(2, 0, 'HV', 'ambix', 'ambix');
        
    
    %[D, S, M, C] = ambi_run_pinv(S,C);
    
    [D, S, M, C] = ambi_run_allrad(S, C, [0,0,1; 0,0,-1]);
    
end

%{
1: L: 105", 30 deg 
2: R: 105", -30 deg
3: C: 91", 0 deg
4: Sub:
5: SL: 60", 70 deg
6: SR: 71", -70 deg
7: RL: 61", 150 deg
8: RR: 55", -150 deg
%}
