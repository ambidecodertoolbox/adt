function run_BK16_MPEG_H
    %
    
    decoder_type = 'ssf'; %'allrad';
    mixed_order_scheme = 'HP';
    channel_order = 'fuma';
    channel_normalization = 'fuma';
    do_plots = [];  % take default
    out_path = [];  % take default
    
    imaginary_speakers = [ ...
        0, 0,  1; % one at the top
        0, 0, -1  % one at the bottom
        ];
    
    
    % their argument is the start angle
    S = SPKR_ARRAY_BatkeKeiler16;
    
    %%
    for h_order = 3
        for v_order = 3
            
            C = ambi_channel_definitions(h_order,v_order,...
                mixed_order_scheme, ...
                channel_order, channel_normalization);
            
            fprintf('h_order = %d, v_order = %d\n', h_order, v_order);
            switch decoder_type
                case 'allrad'
                    [D, S, M, C] = ambi_run_allrad(...
                        S, ...  % speaker array struct
                        C, ...  % channel defs
                        imaginary_speakers, ... % imaginary speakers
                        out_path, ... % pathname for output, [] = default
                        do_plots, ... % do plots, default is true for MATLAB, false for Octave
                        mixed_order_scheme ... % mixed order scheme HV or HP
                        );
                case 'pinv'
                    [D, S, M, C] = ambi_run_pinv(...
                        S, ...  % speaker array struct
                        C, ...  % channel defs
                        [], ... % imaginary speakers, none in this case
                        out_path, ... % pathname for output, [] = default
                        do_plots, ... % do plots, default is true for MATLAB, false for Octave
                        mixed_order_scheme, ... % mixed order scheme HV or HP
                        0.0...
                        );
                case 'ssf'
                    [D, S, M, C] = ambi_run_SSF(...
                        S, ...  % speaker array struct
                        C, ...  % channel defs
                        [], ... % imaginary speakers, none in this case
                        out_path, ... % pathname for output, [] = default
                        do_plots, ... % do plots, default is true for MATLAB, false for Octave
                        mixed_order_scheme, ... % mixed order scheme HV or HP
                        0.0, ...
                        [-45,45] ...
                        );
                    
            end
        end
    end
end


