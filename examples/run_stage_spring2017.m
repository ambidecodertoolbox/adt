function [ D, S, M, C ] = run_stage_spring2017( order )
    %UNTITLED4 Summary of this function goes here
    %   Detailed explanation goes here
    
    if ~exist('order', 'var') || isempty(order)
        order = 6;
    end
    
    C = ambi_channel_definitions(order, order, [], 'acn', 'sn3d');
    %C = ambi_channel_definitions(3, 3, [], 'fuma', 'fuma');
    S = speakers_stage2017();
    
    [D,S,M,C] = ambi_run_allrad(S, C, [0,0,-1], [], true);
    
    D.C = C; D.S = S; 
    
   if isfield(M, 'hf')
       Mx = M.hf;
   else
       Mx =M;
   end
    
    %x = ambi_optimize_matrix2(Mx,C,S);
    
    %ambi_plot_rE(S,[],x,C)
    
    %% make sure jsonlab is on the path
    if ~exist('savejson', 'file')
       addpath(fullfile(ambi_dir('matlab'), 'jsonlab-1.5'));
    end
    
    fid = fopen(fullfile(ambi_dir('examples'), 'SCMD_stage2017.json'), 'w');
    fprintf(fid, savejson('', ...
        struct('S', S, 'C', C, 'M', M, 'D', D)));   
    fclose(fid);
    
    
end

