function [ D, S, M, C ] = run_dec_ITU7_with_square( order )
    %UNTITLED4 Summary of this function goes here
    %   Detailed explanation goes here
    
    if ~exist('order', 'var') || isempty(order)
        order = 1;
    end
    
    % ambix
    %C = ambi_channel_definitions(order, order, [], 'acn', 'sn3d');
    
    % Furse-Malham (FUMA) aka AMB
    
    C = ambi_channel_definitions(order, order, [], 'fuma', 'fuma');
    
    
    S = SPKR_ARRAY_ITU7_with_square;
    
    [D,S,M,C] = ambi_run_allrad(S, C, [0,0,-0.5], [], true);
    
    D.C = C; D.S = S;
    
    if false
        if isfield(M, 'hf')
            Mx = M.hf;
        else
            Mx =M;
        end
        
        x = ambi_optimize_matrix2(Mx,C,S);
        
        ambi_plot_rE(S,[],x,C)
    end
    
    %% make sure jsonlab is on the path
    if ~exist('savejson', 'file')
        addpath(fullfile(ambi_dir('matlab'), 'jsonlab-1.5'));
    end
    
    fid = fopen(fullfile(ambi_dir('examples'), ['SCMD_', S.name, '.json']), 'w');
    fprintf(fid, savejson('', ...
        struct('S', S, 'C', C, 'M', M, 'D', D)));
    fclose(fid);
    
    
end

