function [D, S, M, C] = run_dec_itu( n_spkrs, radius)
    %RUN_DEC_ITU( n_spkrs, radius )
    % Make an AllRAD decoder for an ITU or Dolby Surround speaker array
    %
    % https://www.itu.int/dms_pub/itu-r/opb/rep/R-REP-BS.2159-4-2012-PDF-E.pdf

    
    %% fill in default arguments
    if ~exist('radius', 'var') || isempty(radius)
        radius = 1;  % in meters!!
    end
    
    if ~exist('n_spkrs', 'var') || isempty(n_spkrs)
        n_spkrs = 7;
    end
    
    
    % distance to center speaker, assuming it is in the
    %  plane as the left and right.
    
    [fl_x, fl_y, fl_z] = sph2cart(-30*pi/180, 0, radius);
    
    switch n_spkrs
        case 5
            % ITU -- F: 30, S: 100-120
            % https://www.dolby.com/us/en/guide/surround-sound-speaker-setup/5-1-setup.html
            % Dolby 5.1 -- F: 22-30, S: 90-110, C in same plane as F
            S = ambi_spkr_array(...
                ... % array name
                'itu50', ...
                ... % coordinate codes, unit codes
                ... % Azimuth, Elevation, Radius; Degrees, Degrees, Meters
                'AER', 'DDM', ...
                ... % speaker name, [azimuth, elevation, radius]
                'L',  [  30, 0, radius], ...
                'C',  [   0, 0, fl_x], ...
                'R',  [ -30, 0, radius], ...
                'Ls', [ 110, 0, radius], ...
                'Rs', [-110, 0, radius] ...
                );
        case 7
            % https://www.dolby.com/us/en/guide/surround-sound-speaker-setup/7-1-setup.html
            % Dolby 7.1 F: 22-30, S: 90-110, B: 135-150
            S = ambi_spkr_array(...
                ... % array name
                'itu70', ...
                ... % coordinate codes, unit codes
                ... % Azimuth, Elevation, Radius; Degrees, Degrees, Meters
                'AER', 'DDM', ...
                ... % speaker name, [azimuth, elevation, radius]
                'L',  [  30, 0, radius], ...
                'C',  [   0, 0, fl_x], ...
                'R',  [ -30, 0, radius], ...
                'Ls', [  90, 0, radius], ...
                'Rs', [ -90, 0, radius], ...
                'Lb', [ 150, 0, radius], ...
                'Rb', [-150, 0, radius] ...
                );
        case 10
            % ITU: C: 0; F: 30, W: 60, S: 110, B: 180, H 45AZ, 40-45EL
            
    end
    switch n_spkrs
        case 5
            [D,S,M,C] = ambi_run_allrad(S, [2,0], [0,0,1; 0,0,-1]);
        case 7 
            [D,S,M,C] = ambi_run_allrad(S, [3,3], [0,0,1; 0,0,-1]);
        otherwise
            error('only 5 and 7 speaker supported');
    end
    
    D.C = C; D.S = S; 
    
    %% make sure jsonlab is on the path
    if ~exist('savejson', 'file')
       addpath(fullfile(ambi_dir('matlab'), 'jsonlab-1.5'));
    end
    
    fid = fopen(fullfile(ambi_dir('examples'), ['SCMD_', mfilename, '.json']), 'w');
    fprintf(fid, savejson('', ...
        struct('S', S, 'C', C, 'M', M, 'D', D)));   
    fclose(fid);
end
