function [D, S] = run_example_Dodecahedron(radius)
    % run_example_Dodecahedron(radius) 20-speaker dodecahedron
    %
    % An example specifying speaker locations inline and reading from an
    % an AmbDec preset.
    %
    % A regular dodecahedron as defined at
    %    https://en.wikipedia.org/wiki/Regular_dodecahedron#Cartesian_coordinates
    % This is oriented with an edge at the top that lies in the in the 
    % X-Z (Y=0) plane
    %
    % see also AMBI_MAT2SPKR_ARRAY, AMBI_RUN_ALLRAD
    
    %%
    
    % defaults 
    if ~exist('radius', 'var') || isempty(radius)
        radius = 2;
    end
    
    phi = (1 + sqrt(5))/2;
    
    for h_order = 2:3
        for v_order = h_order %3 %1:min(h_order,3);
            mixed_order_scheme = 'HV';
            
            if true
                S = ambi_mat2spkr_array(...
                    [ ...
                    ... the unit cube
                     1  1  1;
                     1 -1  1;
                    -1 -1  1;
                    -1  1  1;
                    ...
                     1  1 -1;
                     1 -1 -1;
                    -1 -1 -1;
                    -1  1 -1;
                    ... % x-y plane
                     phi  1/phi 0;
                     phi -1/phi 0;
                    -phi -1/phi 0;
                    -phi  1/phi 0;
                    ... % y-z plane
                    0  phi  1/phi;
                    0  phi -1/phi;
                    0 -phi -1/phi;
                    0 -phi  1/phi;
                    ... % x-z plane
                     1/phi 0  phi;
                     1/phi 0 -phi;
                    -1/phi 0 -phi;
                    -1/phi 0  phi;
                    ] / sqrt(3) * radius, ...
                    'XYZ',...            % locations
                    'MMM', ...           % in meters
                    'Dodecahedron' ...   % name of array
                    );
            else
                % ambdec's iscosahedron preset is really a dodecahedron
                S = ambdec2spkr_array(...
                    '~/.ambdecrc/icosahedron-3h3v.ambdec', ...
                    'icosahedron');
            end
            
            %decoder_type = 'allrad';
            decoder_type = 'pinv';
            switch decoder_type
                case 'allrad'
                    D = ambi_run_allrad(...
                        S, ...  % speaker array struct
                        [h_order,v_order], ...  % ambisonic order [h, v]
                        [], ... % imaginary speakers, none in this case
                        [], ... % pathname for output, [] = default
                        true, ... % do plots, default is true for MATLAB, false for Octave
                        mixed_order_scheme ... % mixed order scheme HV or HP
                        );
                case 'pinv'
                    D = ambi_run_pinv(...
                        S, ...  % speaker array struct
                        [h_order,v_order], ...  % ambisonic order [h, v]
                        [], ... % imaginary speakers, none in this case
                        [], ... % pathname for output, [] = default
                        true, ... % do plots, default is true for MATLAB, false for Octave
                        mixed_order_scheme ... % mixed order scheme HV or HP
                        );
            end
        end
    end
    
