[install_dir, ~, ~ ] = fileparts(mfilename('fullpath'));

install_dir = [install_dir '/../adt_initialize'];

run(install_dir);

if inOctave()
    arg_list = argv();

    %unpack arguments
    h_order = str2num(arg_list{1});
    v_order = str2num(arg_list{2});
    alpha = str2num(arg_list{3})
    elevation_range = eval(arg_list{4});
    mixed_order_scheme = arg_list{5};
    ordering_rule = arg_list{6};
    encoding_convention = arg_list{7};
    directions_path = arg_list{8};
    match_type = arg_list{9};
    yml_out_path = arg_list{10};
    out_path = arg_list{11};

    C = ambi_channel_definitions(h_order, v_order, mixed_order_scheme, ordering_rule, encoding_convention);

    S = ambi_mat2spkr_array(directions_path, 'AER', 'RRM');

    [D, S, M, C] = ambi_run_SSF(S, C, [], out_path, false, mixed_order_scheme, alpha, elevation_range);

    write_atk_yml(yml_out_path, D, S, M, C, match_type, 'ssf', [], alpha, elevation_range)

else
    printf('This script can only be run in Octave')
end
